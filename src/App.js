import logo from './logo.svg';
import './App.css';
import Login from "./container/Login";
import Registration from './container/Registration/Registration';
import Navbar from './container/Navbar/Navbar'
import Footer from './container/Footer/Footer'
import About from './container/About/About';
import ContactUs from './container/ContactUs/ContactUs';
import Landing from './container/Landing/Landing';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Services from './container/Services/Services';
import StudentRegistartion from './container/StudentRegistartion/StudentRegistartion';
import CommingSoon from './container/CommingSoon/CommingSoon';
import HomeTuition from './container/ServiceDesc/HomeTuition';
import {ToastContainer} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { Header } from 'antd/lib/layout/layout';
import Imageslider from './container/Imageslider/Imageslider';

function App() {
  return (
    <div className="App">

      {/* <BrowserRouter>
      <Routes>
        <Route path="/" element={<Landing />}>
          <Route path="/blogs" element={<About />} />
          <Route path="/contact" element={<ContactUs />} />
        </Route>
      </Routes>
    </BrowserRouter> */}

    <Router>
        <Routes>
          {/* <Route exact path="/">
            <Redirect to="/login" />
          </Route> */}
          <Route path="/" element={<Landing />}></Route>
          <Route path="/about" element={<About />}></Route>
          <Route path="/contact" element={<ContactUs />}></Route>
          <Route path="/signup" element={<Registration />}></Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/services" element={<Services />}></Route>
          <Route path="/commingSoon" element={<CommingSoon />}></Route>
          <Route path="/homeTuition" element={<HomeTuition />}></Route>
          <Route path="/Landing" element={<Landing />}></Route>
          <Route path="/imageslider" element={<Imageslider />}></Route>
        </Routes>
      </Router>
     
      {/* <Login /> */}
      {/* <Registration /> */}
      {/* <Footer /> */}
      {/* <About /> */}
      {/* <Landing /> */}
      {/* <ContactUs /> */}
      {/* <Navbar /> */}
<div className='container-fluid'>
<Header />
<ToastContainer autoClose={3000} />
</div>
    </div>
  );
}

export default App;
