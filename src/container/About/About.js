import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './About.css'


class About extends Component{
        constructor(props) {
            super(props);
            this.state = {
              name: '',
              email: '',
              message: ''
            }
          }
    render(){
    return (
        <>
         <Navbar />
        <div class="about-section">
  <h1 style={{ textAlign: "center", fontWeight:"bold", fontSize:"25px",color:"yellow" }}>Get To Know Us</h1>
  <p class="about_desc">Tell Us Your Tuition Needs: Just fill few details about your home tuition needs and we will show your requirements to all our tutors.</p>
  <p class="about_desc">Become a teacher by creating your profile here and tell us about yourself, ... our latest home tuition jobs page and start applying for the tuition jobs ...</p>
</div>

<h1 style={{ textAlign: "center", fontWeight:"bold", fontSize:"30px", color:"#4c4c52" }}>Our Industry Leaders</h1>
<div class="row">
  <div class="column">
    <div class="card1">
      <img src={"https://i.postimg.cc/rsMfWR5S/sulabh.jpg"} alt="Sulabh" style={{width:"55%"}} />
      <div class="container">
        <h2 class="name">Sulabh Yadav</h2>
        <p class="title">CEO</p>
        <p class="desc">Sulabh Yadav is known for his soft-spoken, diplomatic nature.</p>
        <p class="mail">sulabh@learn.com</p>
        <p><button class="button" onClick={(e) => {
      e.preventDefault();
      window.open('https://www.linkedin.com/in/sulabh-yadav-43370450/');
      }}>Contact</button></p>
      </div>
    </div>
  </div>

  <div class="column">
    <div class="card2">
      {/* <img src={"https://i.postimg.cc/76fH4b5n/shilu.jpg"} alt="soni" style={{width:"50%"}} /> */}
      <img src={"https://www.w3schools.com/w3images/team1.jpg"} alt="soni" style={{width:"85%"}} />
      <div class="container">
        <h2 class="name">Soni Kumari</h2>
        <p class="title">CTO</p>
        <p class="desc">Soni Kumari responsible for leading the technology or engineering department.</p>
        <p class="mail">soni@learn.com</p>
        <p><button class="button" onClick={(e) => {
      e.preventDefault();
      window.open('https://www.linkedin.com/in/shailendra-yadav-b918a5105/');
      }}>Contact</button></p>
      </div>
    </div>
  </div>

  

  

  
</div>
       
      <Footer />   
        </>
    )
    }
}

export default About
