import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import Imageslider from '../Imageslider/Imageslider';
import './Landing.css'


class Landing extends Component{
        constructor(props) {
            super(props);
            this.state = {
              name: '',
              email: '',
              message: '',
              captionStyle:'',
              slideNumberStyle:''

            }
          }

    render(){
    return (
        <>
        <div className='vert-align'><Navbar /></div>
        
        <div className='image-slider'><Imageslider /></div>
        

   <div className='xyz'>  
  <meta charSet="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
  />
  <style
    // dangerouslySetInnerHTML={{
    //   __html:
    //     '\nhtml,body,h1,h2,h3,h4 {font-family:"Lato", sans-serif}\n.mySlides {display:none}\n.w3-tag, .fa {cursor:pointer}\n.w3-tag {height:15px;width:15px;padding:0;margin-top:6px}\n'
    // }}
  />
  {/* Links (sit on top) */}
  

  {/* Content */}
  <div
    className="w3-content"
    style={{ maxWidth: 1100, marginTop: -82, marginBottom: 80}}>
   
    {/* Slideshow */}
    {/* <div className="w3-container">
    
     
      <div className="w3-container w3-dark-grey w3-padding w3-xlarge"></div>
    </div> */}
    
    {/* Grid */}
    <div className="w3-row w3-container">
      <div className="w3-center w3-padding-64">
        <span className="w3-xlarge w3-bottombar w3-border-dark-grey w3-padding-16">
          What We Offer
        </span>
      </div>
      <div className="w3-col l3 m6 w3-light-white w3-container w3-padding-16">
        
        {/* <h3>Online Tuition</h3>
        <p>
        Online tutoring is practiced using many different approaches for distinct sets of users. The distinctions are in content and user interface, as well as in tutoring styles and tutor-training methodologies.
        </p> */}
      </div>
      <div className="w3-col l3 m6 w3-light-grey w3-container w3-padding-16">
      <h3>Home Tuition</h3>
        <p>
        Home tuition means a tuition teacher or private tutor coming to the home of the student to teach specific subjects. 
        Students are free to choose the timings and the day they want for home tutoring sessions.
        </p>
      </div>
      <div className="w3-col l3 m6 w3-grey w3-container w3-padding-16">
        <h3>Counselling</h3>
        <p>
        Career counselling refers to counselling provided by career counsellors who help individuals choose an appropriate career based on their interests. Manage payroll and insurance administration.
        </p>
        
      </div>
      <div className="w3-col l3 m6 w3-light-white w3-container w3-padding-16">
        {/* <h3>Coaching</h3>
        <p>
        coaching is a process that aims to improve performance and focuses on the 'here and now' rather than on the distant past or future. Coaching is unlocking a person's potential to maximise their own performance.
        </p> */}
      </div>
    </div>
{/* Grid */}
<div className="w3-row-padding" id="plans">
      <div className="w3-center w3-padding-64">
        <h3>Pricing Plans</h3>
        <p>Choose a pricing plan that fits your needs.</p>
      </div>
      <div className="w3-third w3-margin-bottom">
        <ul className="w3-ul w3-border w3-center w3-hover-shadow">
          <li className="w3-black w3-xlarge w3-padding-32">Basic</li>
          <li className="w3-padding-16">
            <b>2 Days</b> in Week
          </li>
          <li className="w3-padding-16">
            <b>1 Subject</b> Daily
          </li>
          <li className="w3-padding-16">
            <b>1 Hour</b> Daily
          </li>
          <li className="w3-padding-16">
            <b>1 Doubt</b> Session
          </li>
          <li className="w3-padding-16">
            <h2 className="w3-wide">₹ 1000</h2>
            <span className="w3-opacity">per month</span>
          </li>
          <li className="w3-light-grey w3-padding-24">
            <button className="w3-button w3-green w3-padding-large">
              Sign Up
            </button>
          </li>
        </ul>
      </div>
      <div className="w3-third w3-margin-bottom">
        <ul className="w3-ul w3-border w3-center w3-hover-shadow">
          <li className="w3-dark-grey w3-xlarge w3-padding-32">Pro</li>
          <li className="w3-padding-16">
          <b>4 Days</b> in Week
          </li>
          <li className="w3-padding-16">
          <b>2 Subject</b> Daily
          </li>
          <li className="w3-padding-16">
          <b>2 Hour</b> Daily
          </li>
          <li className="w3-padding-16">
          <b>2 Doubt</b> Session
          </li>
          <li className="w3-padding-16">
            <h2 className="w3-wide">₹ 2500</h2>
            <span className="w3-opacity">per month</span>
          </li>
          <li className="w3-light-grey w3-padding-24">
            <button className="w3-button w3-green w3-padding-large">
              Sign Up
            </button>
          </li>
        </ul>
      </div>
      <div className="w3-third w3-margin-bottom">
        <ul className="w3-ul w3-border w3-center w3-hover-shadow">
          <li className="w3-black w3-xlarge w3-padding-32">Premium</li>
          <li className="w3-padding-16">
          <b>6 Days</b> in Week
          </li>
          <li className="w3-padding-16">
          <b>3 Subject</b> Daily
          </li>
          <li className="w3-padding-16">
          <b>3 Hour</b> Daily
          </li>
          <li className="w3-padding-16">
          <b>Daily Doubt</b> Session
          </li>
          <li className="w3-padding-16">
            <h2 className="w3-wide">₹ 5000</h2>
            <span className="w3-opacity">per month</span>
          </li>
          <li className="w3-light-grey w3-padding-24">
            <button className="w3-button w3-green w3-padding-large">
              Sign Up
            </button>
          </li>
        </ul>
      </div>
    </div>
    {/* Grid */}
    <div className="w3-row-padding" id="about">
      <div className="w3-center w3-padding-64">
        <span className="w3-xlarge w3-bottombar w3-border-dark-grey w3-padding-16">
          Our Expertise
        </span>
      </div>
      <div className="w3-third w3-margin-bottom">
        <div className="w3-card-4">
          <img src="https://www.w3schools.com/w3images/team1.jpg" alt="Sulabh" style={{ width: "100%" }} />
          <div className="w3-container">
            <h3>Anita Kumari</h3>
            <p className="w3-opacity">English Teacher</p>
            <p>
            She is very experienced English Teacher.
            </p>
            <p>
              <button className="w3-button w3-light-grey w3-block">
                Contact
              </button>
            </p>
          </div>
        </div>
      </div>
      <div className="w3-third w3-margin-bottom">
        <div className="w3-card-4">
          <img src="https://www.w3schools.com/w3images/team2.jpg" alt="Shailendra" style={{ width: "100%" }} />
          <div className="w3-container">
            <h3>Som Kumar</h3>
            <p className="w3-opacity">Math Teacher</p>
            <p>
            He is very experienced Math Teacher.
            </p>
            <p>
              <button className="w3-button w3-light-grey w3-block">
                Contact
              </button>
            </p>
          </div>
        </div>
      </div>
      <div className="w3-third w3-margin-bottom">
        <div className="w3-card-4">
          <img src="https://www.w3schools.com/w3images/team3.jpg" alt="Jane" style={{ width: "100%" }} />
          <div className="w3-container">
            <h3>Ansh Singh</h3>
            <p className="w3-opacity">Chemistry</p>
            <p>
            He is very experienced Chemistry Teacher.
            </p>
            <p>
              <button className="w3-button w3-light-grey w3-block">
                Contact
              </button>
            </p>
          </div>
        </div>
      </div>
    </div>

    
    <header
  className="w3-display-container w3-content w3-wide"
  style={{ maxWidth: 1600, minWidth: 500 }}
  id="home"
>
  <img
    className="w3-image"
    src="https://cdn.pixabay.com/photo/2016/05/30/18/27/workshop-1425446_1280.jpg"
    alt="Hamburger Catering"
    width={1600}
    height={800}
  />
  <div className="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 className="w3-xxlarge"></h1>
  </div>
</header>
    
    </div>   
    
  </div>
 


        
       <div class='footer-icon'>
       <Footer />  
       </div>
      
        </>
    )
    }
}

export default Landing
