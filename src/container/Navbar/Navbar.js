import React ,{ Component }from 'react'
import { Button, Nav, NavLink } from 'react-bootstrap'

import { FaFacebookSquare, FaInstagramSquare, FaYoutubeSquare, FaLinkedin } from "react-icons/fa";
import './Navbar.css'
import BellIcon from 'react-bell-icon';
import About from '../About/About';
import ContactUs from '../ContactUs/ContactUs'
import Registration from '../Registration/Registration';

import {
    BrowserRouter,
    Routes,
    Route,
    Link  } from "react-router-dom"; 

const isTestConnectionLoading = false;
var registerUserId = null
var registerUserId_val = null;
class Navbar extends Component{
    constructor(props) {
        super(props)
        this.state = {
            isTestConnectionLoading: false,
            registerUserId: null
        }
    }
componentDidMount(){
    
    try{
         registerUserId_val = JSON.parse(sessionStorage.getItem('res')).data.pid;
    }catch(e){
console.log("error val123")
    }
    
    this.setState({
       isTestConnectionLoading: true
    })
    console.log("registerUserId ===== "+registerUserId)
    console.log("registerUserId_val ===== "+registerUserId_val)
       }


signup = () =>{
    console.log("happy")
    this.setState({
        isTestConnectionLoading : true
    })
   
}

logout = () => {
    console.log("logout")
    window.localStorage.clear();
    window.sessionStorage.clear();
    //window.location.href = baseUrl + "login";
    window.location.href = "http://localhost:3000";
  }
  
render(){
    return (
        <div>
         <Nav className="main-nav">
           <div className="logo">
               <h2><Link to="/Landing">
                   <span>T</span>UITION
                   <span>w</span>ALA </Link></h2>
           </div>
            <div className="menu-link">
                <ul>
                    <li>
                        {/* <a href="#">Home</a> */}
                        <Link to="/Landing">Home</Link>
                    </li>
                    <li>
                        {/* <a href="#">Services</a> */}
                        <Link to="/services">Services</Link>
                    </li>
                    
                    {registerUserId_val != null ? 
                    <li>
                    {/* <a href="#">About</a> */}
                    <Link to="/about">Profile</Link>
                </li>
                :
                <li>
                    {/* <a href="#">About</a> */}
                    <Link to="/about">About</Link>
                </li>
                    }
                    
                    
                    <li>
                        {/* <a href="#">Contact</a> */}
                        <Link to="/contact">Contact-Us</Link>
                    </li>
                </ul>
            </div>

            <div className="social-media">
                {/* <ul className="social-media-desktop">
                    <li>
                        <a href="https://www.youtube.com/channel/UCvkEjj2pus1lmwbgrE7tugA" target="_rmcor">
                            <FaYoutubeSquare  className="youtube" />
                            </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/rmcor.offical/" target="_rmcor">
                            <FaFacebookSquare className="facebook" />
                            </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/rmcoroffical/" target="_rmcor">
                            <FaInstagramSquare className="instagram" />
                            </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/in/rmcor-offical-4399bb19a/" target="_rmcor">
                            <FaLinkedin className="linkedin"/>
                            </a>

                    </li>
                </ul> */}
            </div>
            {registerUserId_val != null ? 
            <div className="login-btn">
                <Button type="primary" onClick={this.logout} style={{backgroundColor:"red"}}>Logout</Button>
            </div>
:
            <div className="login-btn123">
            <Link to="/signup"><Button type="primary">Sign-Up</Button></Link>
            </div>
}
            {/* hero section
            <section className="hero-section">
            <h1>Welcome to Rmcor.Inc</h1>
            </section> */}
           

        </Nav>   
       
        {this.state.isTestConnectionLoading == true &&
           <Link to="/"></Link>
       }
         </div>    
    )
}
}
export default Navbar
