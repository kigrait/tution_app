import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './Services.css'
import {
	BrowserRouter,
	Routes,
	Route,
	Link  } from "react-router-dom";

class Services extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />


<body>

	<section class="section-services">
		<div class="container">
				<div class="col-md-10 col-lg-8">
					<div class="header-section">
						<h1 class="title"><span>Our</span> Exclusive <span>Services</span></h1>
					</div>
				</div>
			<div class="row">
				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fab fa-500px"></i>
							<h1 class="title">Home Tuition</h1>
						</div>
						<div class="part-2">
							<p class="description">Home tuition means a tuition teacher or private tutor coming to the home of the student to teach specific subjects. Students are free to choose the timings and the day they want for home tutoring sessions. In today’s competitive scenario, every learner requires to perform well in their academics so that they can become successful. </p>
							{/* <a href="#"><i class="fas fa-arrow-circle-right"></i>Read More</a> */}
							<Link to="/homeTuition"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fab fa-angellist"></i>
							<h1 class="title">Online Tuition</h1>
						</div>
						<div class="part-2">
							<p class="description">Online tuition is a way of teaching a student one-on-one, using an online tutoring platform. Tutors may be someone that the student knows personally and has seen in real-life or they may be someone in another country whom they have only ever had online lessons with. Just like in-person lessons, very little is needed for online tuition, only a stable internet connection and a device that has a microphone and video software.</p>
							<Link to="/homeTuition"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fas fa-award"></i>
							<h1 class="title">Coaching</h1>
						</div>
						<div class="part-2">
							<p class="description">coaching is a process that aims to improve performance and focuses on the ‘here and now’ rather than on the distant past or future.
								While there are many different models of coaching, here we are not considering the ‘coach as expert’ but, instead, the coach as a facilitator of learning.</p>
								<Link to="/homeTuition"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fas fa-broadcast-tower"></i>
							<h1 class="title">Counselling</h1>
						</div>
						<div class="part-2">
							<p class="description">Counseling is working with a client who feels uncomfortable, or dissatisfied with their life. They are seeking guidance and advice. A counselor works remedially on a client’s problem.</p>
							<Link to="/homeTuition"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fas fa-broadcast-tower"></i>
							<h1 class="title">Mentoring</h1>
						</div>
						<div class="part-2">
							<p class="description">Mentoring is when a senior colleague, seen as more knowledgeable and worldly wise gives advice and provides a role model. Mentoring involves wide ranging discussions that may not be limited to the work context. A mentor is a sponsor with great professional experience in their client’s field of work. Both mentoring and coaching are concerned mainly with achievements in the present and the future.</p>
							<Link to="/homeTuition"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-4">
					<div class="single-service">
						<div class="part-1">
							<i class="fab fa-canadian-maple-leaf"></i>
							<h1 class="title">Training</h1>
						</div>
						<div class="part-2">
							<p class="description">Training is the process of getting knowledge skills or abilities by study, experience or teaching. The trainer by definition is the expert, and the training course is likely to be targeted on specific skills for immediate results. Training is also likely to be one to many rather than one to one.</p>
							<Link to="/homeTuition"><i class="fas fa-arrow-circle-right"></i>Read More</Link>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>

</body>

      <Footer />   
        </>
    )
      }
}

export default Services
