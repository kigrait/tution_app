import { Carousel } from 'react-carousel-minimal';

function Imageslider() {
  const data = [
    {
      image: "https://i.ibb.co/h20dXsH/1.png",
      caption: ""
    },
    {
      image: "https://i.ibb.co/yqNfBFM/2.png",
      caption: ""
    },
    {
      image: "https://i.ibb.co/Ws3pTdY/3.webp",
      caption: ""
    },
    {
      image: "https://i.ibb.co/pL8mppJ/5.jpg",
      caption: ""
    },
    {
      image: "https://i.ibb.co/jGh9wFn/4.jpg",
      caption: ""
    },
     {
       image: "https://cdn.pixabay.com/photo/2017/01/24/09/18/learn-2004899_1280.jpg",
       caption: ""
     },
     {
       image: "https://i.ibb.co/fGKnKgJ/tuitionlogin.png",
       caption: ""
     },
     {
       image: "https://cdn.pixabay.com/photo/2016/03/12/09/33/map-1251736_1280.jpg",
       caption: ""
     },
     {
       image: "https://cdn.pixabay.com/photo/2019/06/11/08/24/education-4266473_1280.jpg",
       caption: ""
     },
     {
       image: "https://cdn.pixabay.com/photo/2022/01/28/12/17/distance-learning-6974511_1280.jpg",
       caption: ""
     },
     {
       image: "https://cdn.pixabay.com/photo/2016/05/30/18/27/workshop-1425446_1280.jpg",
       caption: ""
     }
   ];
 
   const captionStyle = {
     fontSize: '2em',
     fontWeight: 'bold',
   }
   const slideNumberStyle = {
     fontSize: '20px',
     fontWeight: 'bold',
   }
   return (
     <div className="App">
       <div style={{ textAlign: "center" }}>
         
         <div style={{
           padding: "0 20px"
         }}>
           <Carousel
             data={data}
             time={2000}
             width="100%"
             height="350px"
             captionStyle={captionStyle}
             radius="10px"
             slideNumber={false}
             slideNumberStyle={slideNumberStyle}
             captionPosition="bottom"
             automatic={true}
             dots={false}
             pauseIconColor="white"
             pauseIconSize="40px"
             slideBackgroundColor="darkgrey"
             slideImageFit="cover"
             thumbnails={false}
             thumbnailWidth="100px"
             style={{
               textAlign: "center",
               maxWidth: "100%",
               maxHeight: "500px",
               margin: "40px auto",
             }}
           />
         </div>
       </div>
     </div>
   );
 }
 
 export default Imageslider;

