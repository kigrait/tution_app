import React, { useCallback, useState, Component } from 'react';
import { Button, Nav, NavLink } from 'react-bootstrap'
import validator from 'validator'
import Footer from '../Footer/Footer';
import {
  BrowserRouter,
  Routes,
  Route,
  Link  } from "react-router-dom";
import { render } from '@testing-library/react';
import {toast} from 'react-toastify'


  class Registration extends Component{
    constructor(props) {
      super(props);
      this.state = {
        name: "",
        email: "",
        mobileno: "",
        password: "",
        role:"student"
      };

this.handleInputChange = this.handleInputChange.bind(this);
this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleInputChange(event) {
    event.preventDefault();
    const target = event.target;
    this.setState({
      [target.name]: target.value,
    });
  }

  handleChange = (event) =>{
    var value = event.target.value;
    console.log(value, " was selected");
    this.setState({role: event.target.value});
  }

  refreshPage = () => {
    setTimeout(()=>{
      window.location.reload(false);
  }, 5000);
  }

  handleSubmit = (event) => {
    event.preventDefault();
    console.log("enter "+event);
    console.log("name "+this.state.name);
    console.log("role "+this.state.role);
    this.registrationApiCall()
    
  }

  registrationApiCall = async ()=>{
    console.warn("hello 123")
    var name_val = this.state.name;
    var email_val = this.state.email;
    var mobileno_val = this.state.mobileno;
    var password_val = this.state.password;
    var role_val = this.state.role;
    
  var myBooleanVal = this.checkIfStringHasSpecialChar(email_val);
  console.log("nnnnnnnnnnnnnn "+this.checkIfStringHasSpecialChar(email_val))
  let items = null;
  console.log("happy bbbb"+myBooleanVal)
  items = {firstName:name_val, emailId:email_val,mobileNumber:mobileno_val, password:password_val,role:role_val};
  console.log("value  "+JSON.stringify(items))

  let result = null
  try{
    try{
  result = await fetch("http://localhost:9100/user/registration",{
    method: 'POST',
    headers:{
      "Content-Type":"application/json"
    },
    body: JSON.stringify(items)
  });
}catch(e){
  result = "no result found";
}
  result = await result.json();
  if(result.status == "S10001"){
    toast.success('User Registered Successfully.')
    setTimeout(()=>{
      window.location.href = "http://localhost:3000/";
  }, 3000);
  }else{
    toast.error('Fail to register a user.')
  }
  }
  catch(e){
    console.log("error")
  }
  this.refreshPage()
  }


  checkIfStringHasSpecialChar = (_string) => {
    console.log("_string "+_string)
      let testEmail = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
      if(testEmail.test(_string)){
        return true;
      } else {
        return false;
      }
  }

//   const [firstName, setFirstName] = useState("");
//   const [lastName, setLastName] = useState("");
//   const [mobileNumber, setMobileNumber] = useState("");
//   const [emailId, setEmailId] = useState("");
  

//   const [registrationDetail, setRegistrationDetail] = useState([]);

//   const registrationDetailSubmit=(e) =>{
//     e.preventDefault();
//     const newEntry = {firstName:firstName, lastName:lastName, mobileNumber:mobileNumber, emailId:emailId};
//     setRegistrationDetail([... registrationDetail, newEntry]);
//     console.log(registrationDetail)
//   }
// async function registration(){
//   console.warn("hello 123")
//   let items = {firstName:firstName, lastName:lastName, mobileNumber:mobileNumber, emailId:emailId};
//   console.log("value  "+JSON.stringify(items))
//   let result = await fetch("http://localhost:9100/user/registration",{
//     method: 'POST',
//     headers:{
//       "Content-Type":"application/json"
//     },
//     body: JSON.stringify(items)
//   });
//   result = await result.json();
//   localStorage.setItem("res", JSON.stringify(result))
// }


render(){
return (
  <>
  <div className='nav'>
   {/* <div class="footer-right col-md-4 col-sm-6">
    <div className="logo">
               <h2><a href="/#">
                   <span>T</span>UITION
                   <span>W</span>ALA </a></h2>
           </div>
           </div> */}
    
    {/* <div className="login-btn">
            <Link to="/signin">
                <Button type="primary">SignIn</Button>
                </Link>
            </div> */}
            {/* <div className="login-btn123">
            <Link to="/signup"><Button type="primary">Sign-Up</Button></Link>
            </div> */}



         <Nav className="main-nav" style={{height:"8rem"}}>
           <div className="logo">
               <h2><Link to="/Landing">
                   <span>T</span>UITION
                   <span>w</span>ALA </Link></h2>
           </div>
            <div className="menu-link">
                <ul>
                    {/* <li>
                        <Link to="/Landing">Home</Link>
                    </li>
                    <li>
                        <Link to="/services">Services</Link>
                    </li>
                    
                    {registerUserId_val != null ? 
                    <li>
                    <Link to="/about">Profile</Link>
                </li>
                :
                <li>
                    <Link to="/about">About</Link>
                </li>
                    }
                    
                    
                    <li>
                        <Link to="/contact">Contact-Us</Link>
                    </li> */}
                </ul>
            </div>

            <div className="social-media">
                {/* <ul className="social-media-desktop">
                    <li>
                        <a href="https://www.youtube.com/channel/UCvkEjj2pus1lmwbgrE7tugA" target="_rmcor">
                            <FaYoutubeSquare  className="youtube" />
                            </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/rmcor.offical/" target="_rmcor">
                            <FaFacebookSquare className="facebook" />
                            </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/rmcoroffical/" target="_rmcor">
                            <FaInstagramSquare className="instagram" />
                            </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/in/rmcor-offical-4399bb19a/" target="_rmcor">
                            <FaLinkedin className="linkedin"/>
                            </a>

                    </li>
                </ul> */}
            </div>
           
            <div className="login-btn123">
            <Link to="/login"><Button type="primary" style={{backgroundColor:"rgb(36, 209, 17)",marginleft:"-26rem"}}>Sign-In</Button></Link>
            </div>


           

        </Nav>   
       
      
         
             
  </div>
   <div className="container">
  
  <div className="row">
  <div style={{ textAlign: "center" }}>
    <h2 style={{ textAlign: "center", fontWeight:"bold", fontSize:"25px",color:"#58586a" }}>Create Account</h2>
  </div>
    <div className="column">
    
    <form onSubmit={this.handleSubmit} autoComplete="off">
        <label htmlFor="name" style={{fontWeight:"bold", fontSize:"15px" }}>Name</label>
        <input
              name="name"
              type="text"
              placeholder="Your name..."
              value={this.state.name}
              onChange={this.handleInputChange}
              required="required"
            />
        <label htmlFor="email" style={{fontWeight:"bold", fontSize:"15px" }}>Email</label>
        <input
              name="email"
              type="text"
              placeholder="Your email..."
              value={this.state.email}
              onChange={this.handleInputChange}
            />
        <label htmlFor="mobileno" style={{fontWeight:"bold", fontSize:"15px" }}>Mobile No</label>
        <input
              name="mobileno"
              type="text"
              placeholder="Your mobile no..."
              value={this.state.mobileno}
              onChange={this.handleInputChange}
              required="required"
            />
        <label htmlFor="role" style={{fontWeight:"bold", fontSize:"15px" }}>Role</label>
        <select value={this.state.value} onChange={this.handleChange}>
        <option value="student">Student</option>
          <option value="teacher">Teacher</option>
          <option value="guardian">Guardian</option>
      </select>
        <label htmlFor="password" style={{fontWeight:"bold", fontSize:"15px" }}>Password</label>
        <input
              name="password"
              type="text"
              placeholder="Your password..."
              value={this.state.password}
              onChange={this.handleInputChange}
              required="required"
            />
        <input type="submit" defaultValue="Submit" />
      </form>
      <div style={{ textAlign: "center", fontWeight:"bold", fontSize:"17px",padding: "12px" }}>
    <Link to='/login' className='signin-page-link' style={{ textAlign: "center", fontWeight:"bold", fontSize:"17px",color:"blue" }}>I am already register?</Link>
    </div>
    </div>
    <div className="column">
      {/* <img src="https://cdn.pixabay.com/photo/2017/01/24/09/18/learn-2004899_1280.jpg" style={{ width: "100%" }} /> */}
        <figure>
      <img src="https://i.ibb.co/smMffj8/onlinereggg.jpg" alt="" style={{ width: "100%" }} />
    </figure>
    
    </div>
  </div>
</div>

<header
  className="w3-display-container w3-content w3-wide"
  style={{ maxWidth: 1600, minWidth: 500 }}
  id="home"
>
  <img
    className="w3-image"
    src="https://cdn.pixabay.com/photo/2019/06/11/08/24/education-4266473_1280.jpg"
    alt="Hamburger Catering"
    width={1600}
    height={800}
  />
  <div className="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 className="w3-xxlarge"></h1>
  </div>
</header>

<header
  className="w3-display-container w3-content w3-wide"
  style={{ maxWidth: 1600, minWidth: 500 }}
  id="home"
>
  <img
    className="w3-image"
    src="https://cdn.pixabay.com/photo/2019/12/29/19/35/teaching-4727933_1280.png"
    alt="Hamburger Catering"
    width={1600}
    height={800}
  />
  <div className="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 className="w3-xxlarge"></h1>
  </div>
</header>

<header
  className="w3-display-container w3-content w3-wide"
  style={{ maxWidth: 1600, minWidth: 500 }}
  id="home"
>
  <img
    className="w3-image"
    src="https://cdn.pixabay.com/photo/2012/04/03/13/31/books-25160_1280.png"
    alt="Hamburger Catering"
    width={1600}
    height={800}
  />
  <div className="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 className="w3-xxlarge"></h1>
  </div>
</header>
<header
  className="w3-display-container w3-content w3-wide"
  style={{ maxWidth: 1600, minWidth: 500 }}
  id="home"
>
  <img
    className="w3-image"
    src="https://cdn.pixabay.com/photo/2017/10/21/12/36/training-2874597_1280.jpg"
    alt="Hamburger Catering"
    width={1600}
    height={800}
  />
  <div className="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 className="w3-xxlarge"></h1>
  </div>
</header>
<header
  className="w3-display-container w3-content w3-wide"
  style={{ maxWidth: 1600, minWidth: 500 }}
  id="home"
>
  <img
    className="w3-image"
    src="https://cdn.pixabay.com/photo/2016/05/30/18/27/workshop-1425446_1280.jpg"
    alt="Hamburger Catering"
    width={1600}
    height={800}
  />
  <div className="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 className="w3-xxlarge"></h1>
  </div>
</header>
<header
  className="w3-display-container w3-content w3-wide"
  style={{ maxWidth: 1600, minWidth: 500 }}
  id="home"
>
  <img
    className="w3-image"
    src="https://cdn.pixabay.com/photo/2022/01/28/12/17/distance-learning-6974511_1280.jpg"
    alt="Hamburger Catering"
    width={1600}
    height={800}
  />
  <div className="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 className="w3-xxlarge"></h1>
  </div>
</header>
  </>

)
          }
  }
export default Registration;