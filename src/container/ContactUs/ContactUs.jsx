import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'
import './ContactUs.css'
import {toast} from 'react-toastify'

class ContactUs extends Component{
    constructor(props) {
        super(props);
        this.state = {
          firstname: "",
          emailid: "",
          country: "india",
          subject:"",
          mobileNumber_val:""

        };

        this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleInputChange = (event) => {
        event.preventDefault();
        const target = event.target;
        this.setState({
          [target.name]: target.value,
        });
      }
  
      refreshPage = () => {
        setTimeout(()=>{
          window.location.reload(false);
      }, 3000);
      }

      handleChange = (event) => {
        var value = event.target.value;
        console.log(value, " was selected");
        this.setState({country: event.target.value});
      }

      handleSubmit = (event) => {
        event.preventDefault();
        console.log("enter "+event);
        console.log("firstname "+this.state.firstname);
        console.log("country "+this.state.country);
        this.contactUsApiCall()
        
      }

      contactUsApiCall = async ()=>{
        console.warn("hello 123")
        var firstname_val = this.state.firstname;
        var emailid_val = this.state.emailid;
        var country_val = this.state.country;
        var subject_val = this.state.subject;
        
      var myBooleanVal = this.checkIfStringHasSpecialChar(emailid_val);
      console.log("nnnnnnnnnnnnnn "+this.checkIfStringHasSpecialChar(emailid_val))
      let items = null;
      console.log("happy bbbb"+myBooleanVal)
      var registerUserId_val = null
      try{
         registerUserId_val = JSON.parse(sessionStorage.getItem('res')).data.pid;
      }catch(e){
        console.log("user can't login")
      }
      
      if (myBooleanVal){
        console.log("if")
        items = {name:firstname_val, emailId:emailid_val,country:country_val, subject:subject_val, registeredUserId:registerUserId_val};
      }else{
        console.log("else")
        items = {name:firstname_val,country:country_val, subject:subject_val, mobileNumber:emailid_val, registeredUserId:registerUserId_val};
      }
      
      console.log("value  "+JSON.stringify(items))
      let result = null
      try{
        result = await fetch("http://localhost:9100/user/contactus",{
        method: 'POST',
        headers:{
          "Content-Type":"application/json"
        },
        body: JSON.stringify(items)
      });
      result = await result.json();
      if(result.status == "S10001"){
        toast.success('detail submitted Successfully.')
      }else{
        toast.error('Failed to submit.')
      }
      
    }catch(e){
      console.log('Error')
    }
      this.refreshPage()
      }

      checkIfStringHasSpecialChar = (_string) => {
        console.log("_string "+_string)
          let testEmail = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
          if(testEmail.test(_string)){
            return true;
          } else {
            return false;
          }
      }

      render() {
    return (
        <>
         <Navbar />
         <div class="about-section111">
  <h1 style={{ textAlign: "center", fontWeight:"bold", fontSize:"25px",color:"yellow" }}>We'd love to hear from you.</h1>
  <p class="about_desc">Wheather you have a question about features, trails, pricing, need a demo,or anything else, Our team is</p>
  <p class="about_desc">ready to answer all your question...</p>
  <p style={{ textAlign: "center", fontWeight:"bold", fontSize:"15px",color:"white" }}>Contact No : <span style={{ textAlign: "center", fontWeight:"bold", fontSize:"15px",color:"yellow" }}>(+91) 9794 217 049</span></p>
  <p style={{ textAlign: "center", fontWeight:"bold", fontSize:"15px",color:"white" }}>Email Id : <span style={{ textAlign: "center", fontWeight:"bold", fontSize:"15px",color:"yellow" }}>support@learn.com</span></p>
</div>
         <div className="container">
  <div className="row">
    <div className="column">
      <img src="https://i.ibb.co/SRkLvB8/contactus123.jpg" style={{ width: "100%" }} />
    </div>
    <h2 style={{ textAlign: "center", fontWeight:"bold", fontSize:"25px",color:"#354068f2",textDecorationLine: "underline" }}>Get in Touch with Us</h2>
    <div className="column">
    <form onSubmit={this.handleSubmit} autoComplete="off">
        <label htmlFor="fname" style={{fontWeight:"bold", fontSize:"15px" }}>Name</label>
         <input
              name="firstname"
              type="text"
              placeholder="Your Name....."
              value={this.state.firstname}
              onChange={this.handleInputChange}
            />
        <label htmlFor="email" style={{fontWeight:"bold", fontSize:"15px" }}>Email Id / Mobile No</label>
         <input
              name="emailid"
              type="text"
              placeholder="Your Email / Mobile no....."
              value={this.state.emailid}
              onChange={this.handleInputChange}
            />

       

<label htmlFor="country" style={{fontWeight:"bold", fontSize:"15px" }}>Country</label>
<select value={this.state.value} onChange={this.handleChange}>
        <option value="india">India</option>
          <option value="australia">Australia</option>
          <option value="canada">Canada</option>
          <option value="usa">USA</option>
      </select>
        <label htmlFor="subject" style={{fontWeight:"bold", fontSize:"15px" }}>Subject</label>
         <textarea
              name="subject"
              type="text"
              placeholder="Your Subject....."
              value={this.state.subject}
              onChange={this.handleInputChange}
              required="required"
            />
        <input type="submit" defaultValue="Submit" />
      </form>
    </div>
  </div>
</div>

      <Footer />   
        </>
    )
      }
}

export default ContactUs
