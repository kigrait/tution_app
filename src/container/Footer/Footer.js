import React from 'react'
import './Footer.css'
import CommingSoon from '../CommingSoon/CommingSoon';
import { FaFacebookSquare, FaInstagramSquare, FaYoutubeSquare, FaLinkedin, FaTwitterSquare } from "react-icons/fa";


import {
  BrowserRouter,
  Routes,
  Route,
  Link  } from "react-router-dom";

function Footer() {
    return (
        <>
         <footer class="footer">
  <div class="footer-left col-md-4 col-sm-6">
    <p class="about">
      <span> About the company</span> Rmcor.Inc working on Artificial Intelligence driven platform for Real Estate, Logistics, Medical Science, Education, Travelling and Software Industries etc.
Rmcor.Inc is founded by Shailendra Yadav on 9 October 2017 in New Delhi, India. It's a globally trusted Brand. It's a child company of R M Corporation Of India.
    </p>
    
    <div class="icons">
      <a href="https://www.facebook.com/rmcor.offical/"><FaFacebookSquare className="facebook" /></a>
      <a href="https://twitter.com/rmcoroffical1"><FaTwitterSquare className="instagram" /></a>
      <a href="https://www.linkedin.com/in/rmcor-offical-4399bb19a/"><FaLinkedin className="linkedin"/></a>
      <a href="https://www.youtube.com/channel/UCvkEjj2pus1lmwbgrE7tugA"><FaYoutubeSquare  className="youtube" /></a>
      <a href="https://www.instagram.com/rmcoroffical/"><FaInstagramSquare className="instagram" /></a>
    </div>
  </div>
  <div class="footer-center col-md-4 col-sm-6">
    <div>
      <i class="fa fa-map-marker"></i>
      <p><span> Modi Tower, Nehru Place</span> New Delhi, India</p>
    </div>
    <div>
      <i class="fa fa-phone"></i>
      <p> (+91) 9794 217 049</p>
    </div>
    <div>
      <i class="fa fa-envelope"></i>
      <p><a href="#">tuitionwala@gmail.com</a></p>
    </div>
  </div>
  <div class="footer-right col-md-4 col-sm-6">
    {/* <h2> Company<span> logo</span></h2> */}
    <div className="logo">
               <h2><a href="/Landing">
                   <span>T</span>UITION
                   <span>W</span>ALA </a></h2>
           </div>
    <p class="menu">
      <Link to="/Landing">Home</Link> |
      <Link to="/about">About</Link> |
      <Link to="/services">Services</Link> |
      <Link to="/commingSoon"> News</Link> |
      <Link to="/contact">Contact</Link>
    </p>
    <p class="name">Rmcor.Inc, All rights reserved &copy; 2022</p>
  </div>
</footer>   
        </>
    )
}

export default Footer
