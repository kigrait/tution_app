import React, {Component} from 'react'
import Navbar from '../Navbar/Navbar'
import Footer from '../Footer/Footer'

class HomeTuition extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name: '',
          email: '',
          message: ''
        }
      }
    
      onNameChange(event) {
        this.setState({name: event.target.value})
      }
    
      onEmailChange(event) {
        this.setState({email: event.target.value})
      }
    
      onMessageChange(event) {
        this.setState({message: event.target.value})
      }
    
      handleSubmit(event) {
      }
      render() {
    return (
        <>
         <Navbar />
         <>
  <h1>Home Tuition</h1>
  
  {/* The description is on the same page as the image */}
  <img
    src="https://cdn.pixabay.com/photo/2020/01/22/09/40/teacher-4784917_1280.jpg"
    alt="W3Schools.com"
    width={900}
    height={232}
    longdesc=""
  />
  
  <div id="w3htmlExplained">
    {/* <h2>Image w3html.gif</h2> */}
    
    <h2>Home tuition means a tuition teacher or private tutor coming to the home of the student to teach specific subjects. Students are free to choose the timings and the day they want for home tutoring sessions. In today’s competitive scenario, every learner requires to perform well in their academics so that they can become successful. These days, a lot of offline institutes fail to give personal care to the students, they are not able to make every student understand. Every student is unique, including their learning abilities. Some pupils can comprehend their lessons promptly, while some students need extra help to study effectively. Thus, they can be good for many students who do not feel comfortable asking questions in front of everyone or require special attention for learning. They provide good quality education to the students, in case a student is not able to afford home tuition, they can go for online tuition. Online tuition can be very helpful for students as they can feel comfortable asking doubts and they will get special attention from the online tutor. In both online tuition and home tuition students do not require to go anywhere to get a good education. Many parents hire good tutors for their kids to help in their studies. The main benefit of home tuition is the comfort for the student, they can study from their comfort zone. Students can understand the topics first from the home tutor before the teacher starts in the class.</h2>
  </div>
</>


      <Footer />   
        </>
    )
      }
}

export default HomeTuition
